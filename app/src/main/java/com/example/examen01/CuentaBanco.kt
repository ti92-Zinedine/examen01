package com.example.examen01

class CuentaBanco {
    private var numCuenta = 0
    private var nombre: String? = null
    private var banco: String? = null
    private var saldo = 0f

    // Constructor
    fun CuentaBanco(numCuenta: Int, nombre: String?, banco: String?, saldo: Float){
        this.numCuenta = numCuenta
        this.nombre = nombre
        this.banco = banco
        this.saldo = saldo
    }

    // Métodos del banco
    fun depositar(cantidadD: Float): Float {
        /*this.saldo = cantidadD + this.saldo
        return this.saldo*/
        return cantidadD
    }

    fun retirar(cantidadR: Float, saldoAct: Float): Boolean {
        if(saldoAct < cantidadR){
            return false
        }else {
            return true
        }
    }
}