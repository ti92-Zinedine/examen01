package com.example.examen01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class CuentaBancoActivity : AppCompatActivity() {
    private lateinit var btnDeposito : Button
    private lateinit var btnRetiro : Button
    private lateinit var btnRegresar : Button

    private lateinit var lblBanco : TextView
    private lateinit var lblSaldo : TextView
    private lateinit var lblNombre : TextView

    private lateinit var txtCantidad : EditText

    private var cuentaBanco = CuentaBanco()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuenta_banco)
        iniciarcomponentes()

        // Se obtienen los datos del MainActivity
        var datos = intent.extras
        var nombreBanco = datos!!.getString("banco")
        var nombreCliente = datos!!.getString("nombre")
        var saldoCliente = datos!!.getString("saldo")

        // Se asignan los valores a las etiquetas
        lblBanco.text = nombreBanco.toString()
        lblNombre.text = nombreCliente.toString()
        lblSaldo.text = saldoCliente.toString()


        btnDeposito.setOnClickListener { depositar() }
        btnRetiro.setOnClickListener { retirar() }
        btnRegresar.setOnClickListener { regresar() }
    }

    fun iniciarcomponentes() {
        btnRegresar = findViewById(R.id.btnRegresar)
        btnRetiro = findViewById(R.id.btnRetiro)
        btnDeposito = findViewById(R.id.btnDeposito)

        lblBanco = findViewById(R.id.lblBanco)
        lblNombre = findViewById(R.id.lblNombre)
        lblSaldo = findViewById(R.id.lblSaldo)

        txtCantidad = findViewById(R.id.txtCantidad)
    }

    fun depositar() {
        if(txtCantidad.text.toString().isEmpty()){
            Toast.makeText(this, "Por favor, ingrese una cantidad", Toast.LENGTH_SHORT).show()
        }else {
            var cantidadD = txtCantidad.text.toString().toFloat()
            var nuevoSaldo = cuentaBanco.depositar(cantidadD) + lblSaldo.text.toString().toFloat()
            lblSaldo.setText(nuevoSaldo.toString())
        }
    }

    fun retirar() {
        if(txtCantidad.text.toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese una cantidad", Toast.LENGTH_SHORT).show()
        } else {
            // Asignación de valores a los parámetros
            var cantidadR = txtCantidad.text.toString().toFloat()
            var retiro = cuentaBanco.retirar(cantidadR, lblSaldo.text.toString().toFloat())
            if(retiro == false){
                Toast.makeText(this.applicationContext, "Saldo insuficiente", Toast.LENGTH_SHORT).show()
            }else {
                var nuevoSaldo = (lblSaldo.text.toString().toFloat() - cantidadR)
                lblSaldo.setText(nuevoSaldo.toString())
            }
        }
    }

    fun regresar(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("App Banco")
        confirmar.setMessage("¿Desea regresar a la Pantalla de Inicio?")
        confirmar.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface,which->}
        confirmar.show()
    }
}