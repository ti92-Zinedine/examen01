package com.example.examen01

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private lateinit var txtNumCuenta :EditText
    private lateinit var txtNombre : EditText
    private lateinit var txtBanco : EditText
    private lateinit var txtSaldo : EditText

    private lateinit var btnEnviar : Button
    private lateinit var btnSalir : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarcomponentes()

        btnEnviar.setOnClickListener { enviar() }
        btnSalir.setOnClickListener { salir() }
    }

    private fun iniciarcomponentes() {
        txtNumCuenta = findViewById(R.id.txtNumCuenta)
        txtNombre = findViewById(R.id.txtNombre)
        txtBanco = findViewById(R.id.txtBanco)
        txtSaldo = findViewById(R.id.txtSaldo)

        btnEnviar = findViewById(R.id.btnEnviar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    private fun enviar() {
        if(txtBanco.text.toString().isEmpty() || txtNombre.text.toString().isEmpty() || txtNumCuenta.text.toString().isEmpty() || txtSaldo.text.toString().isEmpty()){
            Toast.makeText(this.applicationContext,"Ingrese los datos faltantes",
                Toast.LENGTH_SHORT).show()
        } else {
            // Hacer un paquete para enviar información
            var bundle = Bundle();
            bundle.putString("banco",txtBanco.text.toString())
            bundle.putString("nombre",txtNombre.text.toString())
            bundle.putString("saldo",txtSaldo.text.toString())

            // Hacer intento para llamar otra actividad
            val intent = Intent(this@MainActivity, CuentaBancoActivity::class.java)
            intent.putExtras(bundle)

            // Iniciar la actividad esperando o no respuesta
            startActivity(intent)
        }
    }

    private fun salir(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("App Banco")
        confirmar.setMessage("¿Desea salir de App banco?")
        confirmar.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface,which->}
        confirmar.show()
    }
}